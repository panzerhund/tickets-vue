export interface PadKeyData {
    value: string;
    disabled: boolean;
}