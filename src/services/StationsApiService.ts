import {StationData} from "@/services/StationData";
import {defer, Deferred} from "q";
import {Vue} from 'vue-property-decorator';


class StationsApiService {
    private static readonly STATIONS_URL: string = "https://raw.githubusercontent.com/abax-as/coding-challenge/master/station_codes.json";
    private static readonly REQUEST_RETRY_TIME: number = 4000;
    private defferedData: Deferred<StationData[]> = defer<StationData[]>();

    public getStationsData(): Q.Promise<StationData[]> {
        this.makeRequest();
        return this.defferedData.promise;
    }

    private makeRequest(): void {
        Vue.axios.get(StationsApiService.STATIONS_URL)
            .then((response) => {
                this.defferedData.resolve(response.data);
            })
            .catch(() => {
                setTimeout(() => this.makeRequest(), StationsApiService.REQUEST_RETRY_TIME);
            });
    }
}

export const stationsApiService = new StationsApiService();