export const CHAR_KEYS = "abcdefghijklmnopqrstuvwxyz ";
export enum SPECIAL_KEYS {
    CLEAR = "clear",
    BACKSPACE = "backspace",
}