import {CHAR_KEYS} from "@/services/PadKeys";
import {StationData} from "@/services/StationData";
import TrieSearch from "trie-search";


export class StationsSearchService {
    private trieSearch = new TrieSearch<StationData[]>("stationName", {splitOnRegEx: false});

    public setData(data: StationData[]): void {
        this.trieSearch.addAll(data)
    }

    public getSearch(word: string): StationData[] {
        return this.trieSearch.get(word);
    }

    public getNextChars(searchResults: StationData[], searchTerm: string): string[] {
        if (searchTerm === "") {
            return CHAR_KEYS.split("");
        }
        const nextChars: string[] = [];
        for (const result of searchResults) {
            const nextChar = result.stationName.charAt(searchTerm.length).toLowerCase();
            if (this.isSupportedChar(nextChar) && nextChars.indexOf(nextChar) < 0) {
                nextChars.push(nextChar);
            }
            if (nextChars.length === CHAR_KEYS.length) {
                break;
            }
        }
        return nextChars;
    }

    private isSupportedChar(char: string): boolean {
        return char !== "" && CHAR_KEYS.indexOf(char) >= 0;
    }
}

export const stationsSearchService = new StationsSearchService();