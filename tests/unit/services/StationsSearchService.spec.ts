import {StationsSearchService} from "@/services/StationsSearchService";
import {expect} from 'chai'

describe('StationsSearchService', () => {
    let testObj: StationsSearchService;
    beforeEach(() => {
        testObj = new StationsSearchService();
    });
    describe('getSearch', () => {
        it('should return stations starting with search term', () => {
            // given
            testObj.setData([
                {
                    stationName: "DARTFORD",
                    stationCode: "DFD"
                },
                {
                    stationName: "DARTON",
                    stationCode: "DRT"
                },
                {
                    stationName: "TOWER HILL",
                    stationCode: "TWH"
                },
                {
                    stationName: "DERBY",
                    stationCode: "DRB"
                },
            ]);
            // when
            const result = testObj.getSearch("DART");
            const result1 = testObj.getSearch("X");
            const result2 = testObj.getSearch("TO");
            // then
            expect(result).to.eql([
                {
                    stationName: "DARTFORD",
                    stationCode: "DFD"
                },
                {
                    stationName: "DARTON",
                    stationCode: "DRT"
                }]);
            expect(result1).to.eql([]);
            expect(result2).to.eql([
                {
                    stationName: "TOWER HILL",
                    stationCode: "TWH"
                }]);
        });

        it('should be case insensitive', () => {
            // given
            testObj.setData([
                {
                    stationName: "dARtford",
                    stationCode: "DFD"
                },
                {
                    stationName: "Darton",
                    stationCode: "DRT"
                },
                {
                    stationName: "TOWER HILL",
                    stationCode: "TWH"
                },
                {
                    stationName: "DERBY",
                    stationCode: "DRB"
                },
            ]);
            // when
            const result = testObj.getSearch("dArT");
            // then
            expect(result).to.eql([
                {
                    stationName: "dARtford",
                    stationCode: "DFD"
                },
                {
                    stationName: "Darton",
                    stationCode: "DRT"
                }]);
        });
    });
    describe('getNextChars', () => {
        it('should return next characters basing on search results', () => {
            // given
            const searchResult = [
                {
                    stationName: "ASH",
                    stationCode: "",
                },
                {
                    stationName: "ASH VALE",
                    stationCode: "",
                },
                {
                    stationName: "ASHBURYS",
                    stationCode: "",
                },
            ];
            // when
            const result = testObj.getNextChars(searchResult, "ASH");
            // then
            expect(result).to.eql([" ", "b"]);
        });
        it('should not duplicate next characters', () => {
            // given
            const searchResult = [
                {
                    stationName: "Camberley",
                    stationCode: "",
                },
                {
                    stationName: "Camborne",
                    stationCode: "",
                },
            ];
            // when
            const result = testObj.getNextChars(searchResult, 'cam');
            // then
            expect(result).to.eql(["b"]);
        });
        it('should return only supported characters', () => {
            // given
            const searchResult = [
                {
                    stationName: "Cam & Dursley",
                    stationCode: "",
                },
            ];
            // when
            const result = testObj.getNextChars(searchResult, "cam ");
            // then
            expect(result).to.eql([]);
        });
        it('should return lower case characters', () => {
            // given
            const searchResult = [
                {
                    stationName: "ABC",
                    stationCode: "",
                },
                {
                    stationName: "ABz",
                    stationCode: "",
                },
            ];
            // when
            const result = testObj.getNextChars(searchResult, "aB");
            // then
            expect(result).to.eql(["c", "z"]);
        });
    });
});
