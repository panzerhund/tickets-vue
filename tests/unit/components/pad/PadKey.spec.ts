import PadKey from '@/components/pad/PadKey.vue';
import {shallowMount} from '@vue/test-utils';
import {expect} from 'chai';

describe('PadKey.vue', () => {
    it('emits char when enabled & clicked', () => {
        const wrapper = shallowMount(PadKey, {
            propsData: {
                symbol: "X",
                value: "x",
                disabled: false,
            }
        });
        wrapper.find(".pad-key").trigger("click");
        expect(wrapper.emitted()).eql({keyClicked: [["x"]]});
    });
    it('should not emit when disabled & clicked', () => {
        const wrapper = shallowMount(PadKey, {
            propsData: {
                symbol: "X",
                value: "x",
                disabled: true,
            }
        });
        wrapper.find(".pad-key").trigger("click");
        expect(wrapper.emitted()).eql({});
    });
    it('should display key', () => {
        const wrapper = shallowMount(PadKey, {
            propsData: {
                symbol: "X",
                value: "x",
                disabled: false,
            }
        });
        expect(wrapper.find(".pad-key").text()).eql("X");
    });
});
