import Pad from '@/components/pad/Pad.vue';
import PadKey from '@/components/pad/PadKey.vue';
import {shallowMount, WrapperArray} from '@vue/test-utils';
import {expect} from 'chai';
import {Vue} from "vue/types/vue";

describe('Pad.vue', () => {
    it('should display keys', () => {
        const allChars = ["a", "b", "c"];
        const padWrapper = shallowMount(Pad, {
            propsData: {
                allChars: allChars,
                nextChars: ["a"],
            }
        });
        const keysComponents: WrapperArray<Vue> = padWrapper.findAllComponents(PadKey);
        allChars.forEach((char: string) => {
            expect(keysComponents.filter((component) => {
                const props = component.props();
                return props.value === char;
            }).length).equals(1);
        })
    });
    it('should set next chars keys enabled', () => {
        const nextChars = ["a", "f", "x"];
        const padWrapper = shallowMount(Pad, {
            propsData: {
                allChars: ["a", "b", "c"],
                nextChars: nextChars,
            }
        });
        padWrapper.vm.charKeys.forEach(keyData => {
            const disabled = nextChars.indexOf(keyData.value) < 0;
            expect(keyData.disabled).equals(disabled);
        })
    });
});
