declare module "trie-search" {
    class TrieSearch<T> {
        constructor(keyFields?: any, options?: any);

        add(obj: T, customKeys: any): any;

        addAll(arr: T, customKeys?: any): void;

        get(key: any): T;

    }

    export = TrieSearch;
}